# frozen_string_literal: true

require "rails_helper"

RSpec.describe Builders::FormResponse::AnswerTextBuilder do
  subject do
    described_class.new(answer_data, response).call
  end

  let(:form) { create(:form) }
  let(:text_type_element) { create(:element, :short_answer, form: form) }
  let(:text_response) { "some valid text" }
  let(:response) { create(:response, form: form) }
  let(:answer_data) do
    {
      element_id: text_type_element&.id,
      answer_text: text_response,
      response: response
    }
  end

  describe "call" do
    context "when receives valid information" do
      it "builds a valid AnswerText element" do
        expect(subject.class).to eql(::AnswerText)
        expect(subject.valid?).to be true
      end
    end

    context "when response is not valid" do
      let(:response) { nil }

      it "builds an invalid AnswerText element" do
        expect(subject.valid?).to be false
      end
    end

    context "when element_id is not valid" do
      let(:text_type_element) { nil }

      it "builds an invalid AnswerText element" do
        expect(subject.valid?).to be false
      end
    end
  end
end
