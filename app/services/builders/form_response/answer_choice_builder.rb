# frozen_string_literal: true

module Builders
  module FormResponse
    # AnswerChoiceBuilder
    class AnswerChoiceBuilder
      include ::Builders::FormResponse::Builder

      def initialize(answer_params, response)
        @answer_params = answer_params
        @response = response
      end

      def call
        build_answer
      end

      private

      attr_reader :answer_params, :response

      def build_answer
        ::AnswerChoice.new(values)
      end

      def values
        {
          element_choice_id: answer_params.dig(:element_choice_id),
          element_id: answer_params.dig(:element_id),
          response: response
        }
      end
    end
  end
end
