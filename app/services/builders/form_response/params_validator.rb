# frozen_string_literal: true

module Builders
  module FormResponse
    # ParamsValidator
    class ParamsValidator < ::ApplicationService
      include ::Builders::FormResponse::ErrorsStructure

      private

      attr_reader :form, :form_elements, :answers_params, :element_ids_from_params

      def call
        validate_required_elements
        validate_elements_belong_to_form
      end

      def initialize(form, answers_params, element_ids_from_params)
        super()
        @form = form
        @form_elements = form.elements
        @answers_params = answers_params
        @element_ids_from_params = element_ids_from_params
        @errors = []
      end

      def validate_required_elements
        required_elements = form_elements.required.ids
        missing_required_elements = required_elements - element_ids_from_params
        return if missing_required_elements.blank?

        error_message = I18n.t(
          "services.builders.forms_response.missing_required_elements",
          elements: missing_required_elements
        )
        add_errors(error_message)
      end

      def validate_elements_belong_to_form
        form_elements_ids = form_elements.ids
        elements_not_from_form = element_ids_from_params - form_elements_ids
        return if elements_not_from_form.blank?

        error_message = I18n.t(
          "services.builders.forms_response.elements_not_belong_to_form",
          elements: elements_not_from_form
        )
        add_errors(error_message)
      end

      def add_errors(error_message)
        add_formated_errors(errors: error_message)
        handle_errors_flow
      end
    end
  end
end
