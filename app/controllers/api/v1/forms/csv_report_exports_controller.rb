# frozen_string_literal: true

module Api
  module V1
    module Forms
      class CsvReportExportsController < ::Api::V1::ApiController
        include ::Api::V1::FormScoped

        def index
          export = Form::CsvReport::Export.new

          render json: serialize_csv_report_export(export)
        end
      end
    end
  end
end
