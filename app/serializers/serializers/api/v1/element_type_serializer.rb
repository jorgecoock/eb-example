# frozen_string_literal: true

module Api
  module V1
    # FormSerializer
    class ElementTypeSerializer
      include JSONAPI::Serializer

      attributes :type_of_answer, :type_of_element
    end
  end
end
