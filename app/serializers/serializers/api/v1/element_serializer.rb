# frozen_string_literal: true

module Api
  module V1
    # ElementSerializer
    class ElementSerializer
      include JSONAPI::Serializer
      extend ::Translatable

      attributes :title, :position, :config, :title_edited_with_answers
      attribute :element_type do |object|
        object.element_type.type_of_element
      end

      has_many :element_choices, lazy_load_data: true, links: {
        related: lambda { |object|
          Rails.application.routes.url_helpers.api_v1_element_element_choices_path(
            element_id: object.id
          )
        }
      }, serializer: lambda { |object|
        translate!(object.decorate)

        ::Api::V1::ElementChoiceSerializer
      }

      belongs_to :form
    end
  end
end
