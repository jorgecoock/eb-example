# frozen_string_literal: true

class Form::Duplicate
  FORM_DUPLICATED_VALUES = [:name, :secondary_organization_id].freeze
  ELEMENT_DUPLICATED_VALUES = [:config, :element_type_id, :position, :title].freeze
  ELEMENT_CHOICE_DUPLICATED_VALUES = [:title].freeze

  attr_reader :duplicated_form

  def initialize(form)
    @form = form
  end

  def duplicate_form_and_children
    @duplicated_form = @form.deep_clone(
      include: { elements: :element_choices },
      only: form_and_children_values
    )
    assign_duplicated_form_name
  end

  private

  attr_writer :duplicated_form

  def form_and_children_values
    [
      *self.class::FORM_DUPLICATED_VALUES,
      {
        elements: [
          *self.class::ELEMENT_DUPLICATED_VALUES,
          { element_choices: [*self.class::ELEMENT_CHOICE_DUPLICATED_VALUES] }
        ]
      }
    ]
  end

  def assign_duplicated_form_name
    @duplicated_form.name = "Copy of #{@duplicated_form.name}".truncate(100)
  end
end
