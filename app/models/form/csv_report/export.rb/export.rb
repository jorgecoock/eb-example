# frozen_string_literal: true

module Form::CsvReport
  class Export
    include ActionCable::PrivateChannel

    attr_reader :id

    def initialize
      super

      @id = action_cable_channel_unique_id
    end

    private

    def channel_name
      FormCsvReportExportChannel.to_s
    end
  end
end
